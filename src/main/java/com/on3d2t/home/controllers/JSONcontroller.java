package com.on3d2t.home.controllers;

import com.jayway.jsonpath.JsonPath;
import com.on3d2t.home.dataCreators.WriteGoodSkus;
import com.on3d2t.home.dataCreators.WriteMissingSkus;
import com.on3d2t.home.workers.TranslitKiller;
import org.apache.commons.io.FileUtils;
import com.on3d2t.home.workers.Downloader;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import static org.apache.commons.io.FileUtils.getTempDirectory;


public class JSONcontroller  {



    static String json;
    static double currency;
    static String timeStamp = new SimpleDateFormat("dd.MM.yyy_HH.mm").format(Calendar.getInstance().getTime());



    public void currency() throws InterruptedException {
        currency = Double.parseDouble((JsonPath.read(json, "@.currency").toString()));
    }


public static void getSkuInfo(List<String> currentSku, String whatWasOrdered) throws IOException, InterruptedException {

    json =  Downloader.download(whatWasOrdered);


    currentSku.forEach((temp) -> {
        String currentSkuID = temp.split(",")[0];
        String currentSkuOrderNum = temp.split(",")[1];
        String quantityInOrder = temp.split(",") [2];
        String sku_code;
        String name;
        String purchase_price;
        double purchase_priceDouble;
        String price;
        int quantityInt;
        String quantity;
        String currentSkuJsonTemp = JsonPath.read(json, "$.products[?(@.sku=='" + currentSkuID + "')]").toString();
        String currentSkuJson = currentSkuJsonTemp.substring(1,currentSkuJsonTemp.lastIndexOf("]"));
            if (currentSkuJson.hashCode() == 0){
                WriteMissingSkus.createMissingSKU(currentSkuOrderNum, currentSkuID,
                        "!!!DON'T EXIST IN MTZ!!!","0", "0",
                        quantityInOrder, "-1", timeStamp);

                }
            else {
                sku_code = JsonPath.read(currentSkuJson, "$.sku").toString();
                name = JsonPath.read(currentSkuJson, "$.name").toString();
                purchase_priceDouble = Double.parseDouble(
                        (JsonPath.read(currentSkuJson, "$.price").toString()).toString()) * currency;
                purchase_price = String.format("%.2f", purchase_priceDouble);
                price = String.format("%.2f", purchase_priceDouble * 1.3);
                quantityInt = Integer.parseInt((JsonPath.read(currentSkuJson, "$.stock").toString()));
                quantity = String.valueOf(quantityInt);
                if (quantityInt == 0) {
                    WriteMissingSkus.createMissingSKU(currentSkuOrderNum, sku_code, name,
                            purchase_price, price, quantityInOrder, quantity, timeStamp);

                } else
                    WriteGoodSkus.createGoodSKU(currentSkuOrderNum, sku_code, name,
                            purchase_price, price, quantityInOrder, quantity, timeStamp);
            }});
    WriteGoodSkus.writeGoodSKU();
    WriteMissingSkus.writeMissingSKU();

    }


}

