package com.on3d2t.home.controllers;

import com.on3d2t.home.workers.TranslitKiller;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class XLScontroller {
    public  File inputTable;

    List<String> importedSkus = new ArrayList<>();
    String whatWasOrdered;


    public void checkInputSkus() throws IOException, InterruptedException {
        // Read XSL file
        FileInputStream inputStream = new FileInputStream(inputTable);

        // Get the workbook instance for XLS file
        HSSFWorkbook workbook = new HSSFWorkbook(inputStream);

        // Get first sheet from the workbook
        HSSFSheet sheet = workbook.getSheetAt(0);

        StringBuffer orderedSkus = new StringBuffer();


        int rowEnd = sheet.getLastRowNum();
        for (int rowNum = 6; rowNum < rowEnd; rowNum++) {
            Row r = sheet.getRow(rowNum);
            if (r == null) {
                // This whole row is empty
                // Handle it as needed
                continue;
            }
            Cell currentSkuOrderNum = r.getCell(1, Row.MissingCellPolicy.RETURN_BLANK_AS_NULL);
            Cell currentSkuCell = r.getCell(2, Row.MissingCellPolicy.RETURN_BLANK_AS_NULL);
            Cell incomeQuantityOfSku = r.getCell(7, Row.MissingCellPolicy.RETURN_BLANK_AS_NULL);
            if (currentSkuCell == null) {
                // The spreadsheet is empty in this cell
            } else {
                String currentSkuCellString;
                int currentSkuOrderNumString;
                int incomeQuantityOfSkuInt;
                String result;

                switch (currentSkuCell.getCellType()) {
                    case NUMERIC:
                        currentSkuCellString = String.valueOf(currentSkuCell.getNumericCellValue()).trim();
                        break;
                    case STRING:
                        currentSkuCellString = currentSkuCell.getStringCellValue().trim();
                        orderedSkus.append( "|" + currentSkuCell.getStringCellValue().trim() );
                        break;
                    default:
                        throw new IllegalStateException("Unexpected value: " + currentSkuCell.getCellType());
                }
                switch (currentSkuOrderNum.getCellType()){
                    case NUMERIC:
                        currentSkuOrderNumString = (int) currentSkuOrderNum.getNumericCellValue();
                        break;
                    case STRING:
                        currentSkuOrderNumString = Integer.parseInt(currentSkuOrderNum.getStringCellValue());
                        break;
                    default:
                        throw new IllegalStateException("Unexpected value: " + currentSkuOrderNum.getCellType());
                }
                incomeQuantityOfSkuInt =  (int) incomeQuantityOfSku.getNumericCellValue();
                result = TranslitKiller.toTranslit(currentSkuCellString) + "," + currentSkuOrderNumString + "," + incomeQuantityOfSkuInt;             ;
                importedSkus.add(result);
            }

        }
        whatWasOrdered = orderedSkus.substring(1, orderedSkus.length());
        JSONcontroller.getSkuInfo(importedSkus, whatWasOrdered);
    }

    private static void createOrderedSKU(){

    }


}
