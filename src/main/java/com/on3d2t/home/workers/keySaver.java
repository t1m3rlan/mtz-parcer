package com.on3d2t.home.workers;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

public class keySaver {

    public static void save(String _url){
        Properties prop = new Properties();
        prop.setProperty("key", _url);
        try {
            prop.store(new FileOutputStream("file.properties"), null);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static String open() throws IOException {
        Properties prop = new Properties();

            prop.load(new FileInputStream("file.properties"));

        return prop.getProperty("key");
    }
}
