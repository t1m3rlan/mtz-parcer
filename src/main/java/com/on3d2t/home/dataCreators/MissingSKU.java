package com.on3d2t.home.dataCreators;

public class MissingSKU {

    public String getCurrentSkuOrderNum() {
        return currentSkuOrderNum;
    }

    public String getSku_code() {
        return sku_code;
    }

    public String getName() {
        return name;
    }

    public String getPurchase_price() {
        return purchase_price;
    }

    public String getPrice() {
        return price;
    }

    public String getQuantityOnStock() {
        return quantityOnStock;
    }

    public String quantityInOrder() {return  quantityInOrder; }

    public String getTimeStamp() {
        return timeStamp;
    }

    private String currentSkuOrderNum;
    private String sku_code;
    private String name;
    private String purchase_price;
    private String price;
    private String quantityOnStock;
    private String quantityInOrder;
    private String timeStamp;

    public MissingSKU(String currentSkuOrderNum, String sku_code,
                      String name, String purchase_price, String price,
                      String quantityInOrder, String quantityOnStock, String timeStamp) {
        this.currentSkuOrderNum =currentSkuOrderNum;
        this.sku_code = sku_code;
        this.name = name;
        this.purchase_price = purchase_price;
        this.price = price;
        this.quantityOnStock = quantityOnStock;
        this.quantityInOrder = quantityInOrder;
        this.timeStamp = timeStamp;
    }



}
