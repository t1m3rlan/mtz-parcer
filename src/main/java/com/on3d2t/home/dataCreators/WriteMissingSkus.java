package com.on3d2t.home.dataCreators;

import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import org.apache.poi.hssf.usermodel.HSSFDataFormat;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;

import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class WriteMissingSkus {

    public static String currentFileToWrite;
    private static String[] columns = {"order_id", "sku_code", "name", "price", "quantity", "purchase_price", "total", "update_datetime"};
    private static List<MissingSKU> missingSKUS = new ArrayList<>();
    private static ArrayList  <Double> prices = new ArrayList<>();


    public static void createMissingSKU(String currentSkuOrderNum, String sku_code, String name,
                                        String purchase_price, String price,
                                        String quantityInOrder, String quantity, String timeStamp) {
        missingSKUS.add(new MissingSKU(currentSkuOrderNum,  sku_code, name,
                purchase_price, price,
                quantityInOrder, quantity, timeStamp));
    }

    public static void writeMissingSKU( ) {
        // Create a Workbook
        Workbook workbook = new HSSFWorkbook(); // new HSSFWorkbook() for generating `.xls` file

        /* CreationHelper helps us create instances of various things like DataFormat,
           Hyperlink, RichTextString etc, in a format (HSSF, XSSF) independent way */
        CreationHelper createHelper = workbook.getCreationHelper();

        // Create a Sheet
        Sheet sheet = workbook.createSheet("Missing");

        // Create a Font for styling header cells
        Font headerFont = workbook.createFont();
        headerFont.setBold(true);
        headerFont.setFontHeightInPoints((short) 12);
        headerFont.setColor(IndexedColors.BLACK.getIndex());

        // Create a CellStyle with the font
        CellStyle headerCellStyle = workbook.createCellStyle();
        headerCellStyle.setFont(headerFont);

        // Create a Row
        Row headerRow = sheet.createRow(0);

        // Create cells
        for (int i = 0; i < columns.length; i++) {
            Cell cell = headerRow.createCell(i);
            cell.setCellValue(columns[i]);
            cell.setCellStyle(headerCellStyle);
        }

        // Create Cell Style for formatting Date
        CellStyle dateCellStyle = workbook.createCellStyle();
        dateCellStyle.setDataFormat(createHelper.createDataFormat().getFormat("dd-MM-yyyy"));
        CellStyle style = workbook.createCellStyle();
        style.setDataFormat(HSSFDataFormat.getBuiltinFormat("0.00"));


        //setting index of first row to write
        int rowNum = 1;
        for (MissingSKU currentWorkingSku : missingSKUS) {


            Row row = sheet.createRow(rowNum++);

            String orderNum = currentWorkingSku.getCurrentSkuOrderNum().
                    substring(0, currentWorkingSku.getCurrentSkuOrderNum().length() - 2);
            row.createCell(0)
                    .setCellValue(orderNum);

            row.createCell(1)
                    .setCellValue(currentWorkingSku.getSku_code());

            row.createCell(2)
                    .setCellValue(currentWorkingSku.getName());


            row.createCell(3)
                    .setCellValue(currentWorkingSku.getPrice());

            row.createCell(4)
                    .setCellValue(currentWorkingSku.quantityInOrder());

            row.createCell(5)
                    .setCellValue(currentWorkingSku.getPurchase_price());


            row.createCell(7)
                    .setCellValue(currentWorkingSku.getTimeStamp());
            row.getCell(7).setCellStyle(dateCellStyle);


            String totalSumValue = ("SUM(F2:F" + (missingSKUS.size() + 1) + ")");
            sheet.getRow(rowNum - 1).createCell(6).setCellFormula(totalSumValue);

            //making "price" cells more readable;
            Cell cellStyler = row.getCell(5);
            cellStyler.setCellStyle(style);
        }
        //Resize all columns due to data lenth
        for (int i = 0; i < columns.length; i++) {
            sheet.autoSizeColumn(i);
        }
        //write total sum of order.
        int totalRows = sheet.getPhysicalNumberOfRows();
        for (int y = 1; y < totalRows; y++) {
            DecimalFormat f = new DecimalFormat("##.00");
            String sum = f.format(getSum());
            String finalSum = sum;
            sheet.getRow(y).createCell(6).setCellValue(finalSum);
        }
        // Write the output to a file
        FileOutputStream fileOut = null;
        try {
            fileOut = new FileOutputStream(currentFileToWrite);
            workbook.write(fileOut);
            fileOut.close();
            workbook.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("Missing done");
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Information Dialog");
        alert.setHeaderText("Look, an Information Dialog");
        alert.setContentText("I have a great message for you!");
        Optional<ButtonType> result = alert.showAndWait();
        if(!result.isPresent())
        System.exit(0);
else if(result.get() == ButtonType.OK)
            System.exit(0);

        alert.showAndWait();
        Thread.currentThread().interrupt();

    }
    public static double getSum()
    {
        double sum = 0;
        for(int i = 0; i < prices.size(); i++)
        {
            sum += prices.get(i);
        }
        return sum;
    }


}
