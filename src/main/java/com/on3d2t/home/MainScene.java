package com.on3d2t.home;

import com.on3d2t.home.controllers.XLScontroller;
import com.on3d2t.home.dataCreators.WriteGoodSkus;
import com.on3d2t.home.dataCreators.WriteMissingSkus;
import com.on3d2t.home.workers.keySaver;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;

import java.io.File;
import java.io.IOException;

public class MainScene {



    @FXML
    private MenuItem about;

    @FXML
    private Button btn;

    @FXML
    private MenuItem close;

    @FXML
    private AnchorPane mainScene;

    @FXML
    private ProgressIndicator prgrCircle;

    @FXML
    private TextField pass;

    @FXML
    private Label label;

    @FXML
    public void initialize() {
        //pass.setText("Enter API KEY FIRST");

        try {
            pass.setText(keySaver.open());
        } catch (IOException e) {
            pass.setText("Enter API KEY FIRST");
        }
    }





    @FXML
    void clickBtn(ActionEvent event) throws IOException {
        String filePath;

        prgrCircle.setVisible(true);
        keySaver.save(pass.getText());
        FileChooser fileChooser = new FileChooser();
        FileChooser.ExtensionFilter extFilter =
                new FileChooser.ExtensionFilter("XLS files (*.xls)", "*.xls");
        fileChooser.setInitialDirectory(
                new File(System.getProperty("user.home") + "/Desktop"));
        fileChooser.getExtensionFilters().add(extFilter);
        File file = fileChooser.showOpenDialog(mainScene.getScene().getWindow());

        if (file != null) {



            WriteMissingSkus.currentFileToWrite = file.getParent() + "\\missingSKU.xls";
            WriteGoodSkus.currentFileToWrite = file.getParent() + "\\goodSKU.xls";
            //directoryChooser.setInitialDirectory(new File(filePath));

        }
        XLScontroller startReadXlx = new XLScontroller();
        startReadXlx.inputTable = file;



            try {

            prgrCircle.setVisible(true);



                startReadXlx.checkInputSkus();


            } catch (IOException | InterruptedException e) {
                e.printStackTrace();
            }
        };




    @FXML
    void clickClose(ActionEvent event) {
        System.exit(0);

    }

    @FXML
    void clickAbout(ActionEvent event) {

    }



}