package com.on3d2t.home;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;

public class ExitScene {

        @FXML
        private Label detailsLabel;

        @FXML
        private HBox actionParent;

        @FXML
        private Button okButton;

        @FXML
        private HBox okParent;

        @FXML
        private Label messageLabel;

        @FXML
        void close(ActionEvent event) {
            System.exit(0);

        }
 }

